﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System;

namespace BadLab.Tests
{
    public class AutomationCore
    {
        private IWebDriver _driver;
        public AutomationCore()
        {
            _driver = new ChromeDriver();
        }

        [Test]
        public void BasketTest()
        {
            _driver.Navigate().GoToUrl("https://badlab.ru/");
            var products = _driver.FindElement(By.Id("freewall"));
            Assert.IsNull(products);
            _driver.Close();
            _driver.Quit();
        }
    }
}
