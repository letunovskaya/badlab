using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace BadLab.UnitTests
{
    [TestClass]
    public class UnitTest
    {
        private IWebDriver _driver;
        public UnitTest()
        {
            var applicationFolder = AppDomain.CurrentDomain.BaseDirectory;// Get current directory
            _driver = new ChromeDriver(applicationFolder);// Create ChromeDriver instance
        }
        [TestMethod]
        public void BasketTest()
        {
            try
            {
                _driver.Navigate().GoToUrl("https://badlab.ru/");// Go to the site
                var productContainer = _driver.FindElement(By.Id("freewall"));// Find the container with products
                var products = productContainer.FindElements(By.ClassName("cell"));// Find all products in this container
                var firstProduct = products[0];// Find the first product in this container
                var overlay = firstProduct.FindElement(By.ClassName("overlay"));// Find a darkening background on this product
                IJavaScriptExecutor js = _driver as IJavaScriptExecutor;
                js.ExecuteScript("arguments[0].style='opacity: 1;'", overlay);// Make visible the dark background
                var buyButton = firstProduct
                    .FindElement(By.ClassName("button-group"))// Find element where "buy" and "star" button are located
                    .FindElement(By.ClassName("btn-primary"));// Find button with basket
                js.ExecuteScript("arguments[0].click();", buyButton);// Click busket button
                var originPrice = firstProduct.FindElement(By.ClassName("price"));// Find Origin price
                var basketPopup = _driver.FindElement(By.Id("cart"));// Find pop up with basket
                js.ExecuteScript("arguments[0].className += ' opendrop';", basketPopup);// Make visible this pop up
                var basketPrice = basketPopup.FindElement(By.ClassName("closeCart")).FindElement(By.ClassName("sum"));// Find the price that is added to the basket
                Assert.AreEqual(originPrice?.Text, basketPrice?.Text);// Compare Origin and the price that is added to the basket
            }
            catch (Exception ex)
            {
                Assert.Fail();// If error => test fail
            }
        }

    }
}
